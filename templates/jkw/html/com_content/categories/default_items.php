<?php



/**
 * @version		$Id: default_items.php 20196 2011-01-09 02:40:25Z ian $
 * @package		Joomla.Site
 * @subpackage	com_content
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */



// no direct access

defined('_JEXEC') or die;

$class = ' class="first"';

if (count($this->items[$this->parent->id]) > 0 && $this->maxLevelcat != 0) :

?>
<h1 class="shadowed_title">
	<?php echo $this->escape($this->params->get('page_heading')); ?>
</h1>
<div class="darker item clr clearfix">
<ul class="archive std">

<?php foreach($this->items[$this->parent->id] as $id => $item) : ?>

	<?php

	if ($this->params->get('show_empty_categories_cat') || $item->numitems || count($item->getChildren())) :

	if (!isset($this->items[$this->parent->id][$id + 1]))

	{

		$class = ' class="last"';

	}

	?>

	<li<?php echo $class; ?>>
	
	<?php 
	$class = ''; 
	$image=$item->getParams()->get('image');
	$pos = strpos($image, '.');
	if($pos !== false) { ?>
		<div class="left categories-item-image"><img class="left " src="<?php echo  $image ?>" alt="<?php  echo $this->escape($item->title);  ?>" /></div>
	<?php } ?> 
		
		<h2 class="item-page-title"><a href="<?php echo JRoute::_(ContentHelperRoute::getCategoryRoute($item->id));?>">

			<?php echo $this->escape($item->title); ?></a>

		</h2>

		<?php if ($this->params->get('show_cat_num_articles_cat') == 1) :?>

			<span class="article-count">

            	<span><?php echo JText::_('COM_CONTENT_NUM_ITEMS'); ?></span>

				<span><?php echo $item->numitems; ?></span>

			</span>

		<?php endif; ?>

        

		<?php if ($this->params->get('show_subcat_desc_cat') == 1) :?>

		<?php if ($item->description) : ?>

			<div class="category-desc">

				<?php echo JHtml::_('content.prepare', $item->description); ?>

			</div>

		<?php endif; ?>

        <?php endif; ?>



		<?php if (count($item->getChildren()) > 0) :

			$this->items[$item->id] = $item->getChildren();

			$this->parent = $item;

			$this->maxLevelcat--;

			echo $this->loadTemplate('items');

			$this->parent = $item->getParent();

			$this->maxLevelcat++;

		endif; ?>
	<br class="clr" />
	</li>

	<?php endif; ?>

<?php endforeach; ?>

</ul>
</div>
<?php endif; ?>
