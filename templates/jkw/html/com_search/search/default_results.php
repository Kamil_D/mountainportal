<?php
/**
 * @package		Joomla.Site
 * @subpackage	com_search
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

$searchword = urldecode($_GET['searchword']);
$searchwordArray = explode(' ',$searchword);
$searchwordArray[] = $searchword;
$searchwordArrayR = array();
	foreach ($searchwordArray as $v) {
		$searchwordArrayR[] = '<b>'.$v.'</b>';
	}


function colorize($searchwordArray,$searchwordArrayR,$text) {


	return str_replace($searchwordArray, $searchwordArrayR, $text);
}

?>

<dl class="readable search-results<?php echo $this->pageclass_sfx; ?>">
<?php foreach($this->results as $result) : ?>
	
	<dt class="result-title padding10 ">
		<h4><?php echo $this->pagination->limitstart + $result->count.'. ';?>
			<?php if ($result->href) :?>
				<a href="<?php echo JRoute::_($result->href); ?>"<?php if ($result->browsernav == 1) :?> target="_blank"<?php endif;?>>
					<?php echo $this->escape($result->title);?>
				</a>
			<?php else:?>
				<?php echo $this->escape($result->title);?>
			<?php endif; ?>
			<?php if ($this->params->get('show_date')) : ?>
				
		</h4>
		<?php endif; ?>
		<?php if ($result->section) : ?>
			
			<small class="right small<?php echo $this->pageclass_sfx; ?>">
				[<?php echo $this->escape($result->section); ?>]
			</small>
		
	<?php endif; ?>
	</dt>
	
	<p class="result-text">
		<strong class=" muted result-created<?php echo $this->pageclass_sfx; ?>">
			<?php echo JText::sprintf( $result->created); ?>
		</strong>
		<?php// echo colorize($searchwordArray,$searchwordArrayR,$result->text); ?>
		<?php echo $result->text;?>
	</p>
	
<?php endforeach; ?>
</dl>

<div class="pagination">
	<?php echo $this->pagination->getPagesLinks(); ?>
</div>
