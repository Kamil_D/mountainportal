<?php /**
 * @package		Joomla.Site
 * @subpackage	Templates.atomic
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die ;
//JHtml::_('behavior.keepalive');
?>
<?php if ($type == 'logout') : ?>
<div id="logedContainer">
    
<form action="index.php" method="post" id="form-login-show">
<?php if ($params->get('greeting')) : ?>

	<?php
	if ($params -> get('name') == 0) : {
			echo JText::sprintf('MOD_LOGIN_HINAME', $user -> get('name'));
		}
	else : {
			echo JText::sprintf('MOD_LOGIN_HINAME', $user -> get('username'));
		}
	endif;
 ?>

<?php endif; ?>


	<?php
	$user_tmp = JFactory::getUser();
	if ($user_tmp -> id) {
		echo '<div ><p class="left ">
        <img style="max-width:50px;max-height:50px;" src="http://id.portalgorski.pl/index.php?co=44&akcja=pokaz&id=' . md5('_.34*m@Y3s' . $user_tmp -> username) . '" alt="avatar"/>
        </p><p class="left ">Hello <a href="http://id.portalgorski.pl/index.php" title="Click to view and edit your profile">'. $user_tmp -> get('username').' </a>! ';
	    ?> <br/><input type="submit" name="Submit" class="button" value="<?php echo JText::_('JLOGOUT'); ?>" />  <?php
		echo' </p></div>';
		
	}
	?>
	
	<input type="hidden" name="option" value="com_users" />
	<input type="hidden" name="task" value="user.logout" />
	<input type="hidden" name="return" value="" />
	<?php echo JHtml::_('form.token'); ?>


</form>
<div class="clr"></div>
</div>
<?php else : ?>
    
<div id="loginContainer">
         <div>
              <a class="button login-dialog right"  >Log in</a>
                <a class="button-inverse left" href="http://id.portalgorski.pl/index.php?co=35&id=3" >Register</a>
        </div>
   
    <form action="<?php echo JRoute::_('index.php', true, $params -> get('usesecure')); ?>" method="post" id="form-login" class="background standard" title="Log in">
        <fieldset>
            
          <a class="right button-inverse" href="http://id.portalgorski.pl/index.php">Log in via Facebook / Google</a>
            <br/>
        </fieldset>
        
    	<?php echo $params -> get('pretext'); ?>
    	
    	
    	<fieldset class="input login-fieldset" >
    	<p>
    		<label for="modlgn_username"><?php echo JText::_('MOD_LOGIN_VALUE_USERNAME') ?></label>
    		<input id="modlgn_username" type="text" name="username" class="inputbox right"  size="18"  />
    	</p><p>
    		<label for="modlgn_passwd"><?php echo JText::_('JGLOBAL_PASSWORD') ?></label>
    		<input id="modlgn_passwd" type="password" name="password" class="inputbox right" size="18"  />
    	</p>
    	<p>
    
    	<?php if (JPluginHelper::isEnabled('system', 'remember')) : ?>
        
    		<label for="modlgn_remember"><?php echo JText::_('MOD_LOGIN_REMEMBER_ME') ?></label>
    		<input  id="modlgn_remember" type="checkbox" name="remember" class="inputbox" value="yes"/>
    
    	<?php endif; ?>
    	</p>
    	<p>
    		<label></label>
    		<input class="right" type="submit" name="Submit" class="button" value="<?php echo JText::_('JLOGIN') ?>" />
    	</p>
    
    	<span>
    		<input type="hidden" name="option" value="com_users" />
    		<input type="hidden" name="task" value="user.login" />
    		<input type="hidden" name="return" value="<?php echo $return; ?>" />
    		<?php echo JHtml::_('form.token'); ?>
    
    	</span>
    	</fieldset>
        
            <?php echo $params -> get('posttext'); ?>
            <p style="font-size:0.9em">Don't have an account? <strong> <a  class="" href="http://id.portalgorski.pl/index.php?co=35&id=3">Register</a></strong><br/>
                                   Forgot your password?<strong> <a  class="" href="http://id.portalgorski.pl/index.php?co=45">Reset password</a></strong>
            </p>
          
    	
    </form>
	<div class="clr"></div>
</div>

<?php endif; ?>