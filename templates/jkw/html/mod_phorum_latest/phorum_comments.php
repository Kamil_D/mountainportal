<?php

class CommentHelper {
	private $content_id;
	private $content_title;
	private $subforum_id = 1;
	private $module = 'jkw';
	private $linkbase = 'http://www.portalgorski.pl/forum/';
	private $avatar_linkbase = 'http://id.portalgorski.pl/index.php?co=44&akcja=pokaz&id=';

	public function __construct($id, $title,$module='jkw') {
		$this -> content_id = (int)$id;
		$this -> content_title = $title;
        $this->module=$module;
	}

	public function link($to, $id) {
		switch($to) {
			case 'avatar' :
				$link = $this -> avatar_linkbase . md5('_.34*m@Y3s' . $id);
				break;
			case 'author' :
				$link = $this -> linkbase . 'profile.php?' . $this -> subforum_id . ',' . $id;
				break;
			case 'responses' :
				$link = $this -> linkbase . 'read.php?' . $this -> subforum_id . ',' . $id . ',' . $id;
				break;
			case 'add' :
				$comment_info = strtr(base64_encode(serialize(array("id" => $this -> content_id, "topic" => $this -> content_title, "jkw_module" => $this -> module, ))), '+/=', '-_|');
				$link = $link = $this -> linkbase . 'posting.php?' . $this -> subforum_id . ',comment=' . $comment_info;
				break;
		}

		return $link;
	}

	public function prepareBody(&$body) {
		$pos = strpos($body, '[i]Jest to komentarz');

		if ($pos === false) {
			$body = str_replace("#", "//", str_replace("/", "/<br/>", str_replace("//", "#", preg_replace("|\[/*[a-z][^\]]*\]|i", "", $body))));
			return $body;
		} else {
			$body = substr(mb_stristr($body, '[i]Jest to komentarz', true), 0, 355);
			$body = str_replace("#", "//", str_replace("/", "/<br/>", str_replace("//", "#", preg_replace("|\[/*[a-z][^\]]*\]|i", "", $body))));
			return $body;
		}
	}

	public function prepareTopic(&$subject) {

		$subject = str_replace('Komentarz do:', '', $subject);
		//echo $subject;
		$subject = str_replace("#", "//", str_replace("/", "/", str_replace("//", "#", preg_replace("|\[/*[a-z][^\]]*\]|i", "", $subject))));

		return $subject;

	}

	public function render() {

		$output = '   <ul class="table"> ';
		$comments_url = 'http://portalgorski.pl/forum/comments.php?mod=' . $this -> module . '&id=' . $this -> content_id;

		libxml_use_internal_errors(true);
		try {
			$comments = new SimpleXMLElement($comments_url, NULL, TRUE);
		} catch (Exception $e) {
			echo '<li>Brak komentarzy...</li>';

		}

		if (($comments -> thread)) {
			foreach ($comments->thread as $comment) {

				$output .= '<li>
				<img class="comments_avatar" src="' . $this -> link('avatar', $comment -> author) . '" alt="' . $comment -> author . '" />';
				$output .= '<h3>
				<a href="' . $this -> link('author', $comment -> user_id) . '">' . $comment -> author . '</a></h3>
				<small>(' . date("j.n.Y G:i", intval($comment -> datestamp)) . ')</small><h5><a   href="' . $this -> link('responses', $comment -> message_id) . '" title="Przeczytaj cały wątek na forum">				   ' . $this -> prepareTopic($comment -> subject) . ' </a></h5>';
				$output .= '' . $this -> prepareBody($comment -> body) . '';
				$output .= '<a href="' . $this -> link('responses', $comment -> message_id) . '" title="Przeczytaj cały komentarz..">[...]</a><br/>';
				$output .= '<small><a href="' . $this -> link('responses', $comment -> message_id) . '" title="zobacz odpowiedzi w tym wątku">
				odpowiedzi (' . ($comment -> thread_count - 1) . ')</a></small>';
				$output .= '</li><li>&nbsp;</li>';

			}

		}
		$output .= '</ul><br/>
			<a class="button" href="' . $this -> link('add', 0) . '" >Dodaj komentarz</a><br/><br/>';

		echo $output;
	}

}
?>