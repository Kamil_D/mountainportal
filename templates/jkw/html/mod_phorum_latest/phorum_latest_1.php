<?php

class CommentHelper {
	private $content_id;
	private $content_title;
	private $subforum_id=1;
	private $module='jkw';
	private $linkbase = 'http://www.portalgorski.pl/forum/';
	
	public function __construct($id='',$title='') {
		$this->content_id='';
		$this->content_title=$title;
	}
	
	public function link($to,$id) {
		switch($to) {
			case 'author' : $link=$this->linkbase.'profile.php?'.$this->subforum_id.','.$id; break;
			case 'responses' : $link=$this->linkbase.'read.php?'.$this->subforum_id.','.$id.','.$id;break;
			case 'add' : $comment_info = strtr(base64_encode(serialize(
					array("id"=>$this->content_id, "topic"=>$this->content_title,"jkw_module" => $this->module,)
											)),'+/=', '-_|');
				$link=$link=$this->linkbase.'posting.php?'.$this->subforum_id.',comment='.$comment_info;
			break;
		}
		
		
		return $link;
	}
	
	public function prepareBody(&$body) {
		
		$pos = strpos($body,'[i]Jest to komentarz');

		if($pos === false) {
			$body=  str_replace("#", "//", str_replace("/", "/<br/>", str_replace("//", "#", preg_replace( "|\[/*[a-z][^\]]*\]|i", "", $body))));
			$body= mb_substr($body,0,55);
			return $body;
		} else {
			$body=  str_replace("#", "//", str_replace("/", "/<br/>", str_replace("//", "#", preg_replace( "|\[/*[a-z][^\]]*\]|i", "", $body))));
			$body= mb_substr(mb_stristr($body,'Jest to komentarz',true),0,55);
			return $body;
		}
		
		
	}
	
	public function prepareTopic(&$subject) {
		
		$subject=mb_substr(str_replace('Komentarz do:', '', $subject),0,30).'...';
		//echo $subject;
		$subject=  str_replace("#", "//", str_replace("/", "/<br/>", str_replace("//", "#", preg_replace( "|\[/*[a-z][^\]]*\]|i", "", $subject))));
		
		return $subject;
		
	}
	
	public function render() {
	
	$output = '<div class="comments"  >   <ul class=""> ';
	$comments_url = $this->linkbase.'comments.php?mod=newest&id='.$this->content_id;
	//echo $comments_url;
	libxml_use_internal_errors(true); 
	try{ 
		$comments = new SimpleXMLElement($comments_url, NULL, TRUE);
	} catch (Exception $e){ 
		echo 'Brak połączenia z forum...'; 
	
	}
	
	if(($comments->thread)) {
		
		foreach ($comments->thread as $comment) {
			
			$output .= '<li>';
			$output .= '<h5><a   href="'.$this->link('responses',$comment->message_id).'" title="Przeczytaj cały wątek na forum">
				   '.$this->prepareTopic($comment->subject).' </a><a class="button"  href="'.$this->link('responses',$comment->message_id).'" title="Przeczytaj cały komentarz..">&raquo;</a></h5>';
			$output .= '<p>&nbsp;'.$this->prepareBody($comment->body).'...';
			
			$output .= '<small >
				'.date("j.n.Y G:i", intval($comment->datestamp)).' przez <a href="'.$this->link('author',$comment->user_id).'">'.$comment->author.'</a></small>';
			
			//$output .= '<a href="'.$this->link('responses',$comment->message_id).'" title="zobacz odpowiedzi w tym wątku">
			//	odpowiedzi ('.($comment->thread_count-1).')</a></small>';
			$output .='</p></li>';
			
			
		}

	} 
	$output .= '</ul>';
	$output .='</div>';
	echo $output;
	}

}

$comments = new CommentHelper();
$comments->render();

?>