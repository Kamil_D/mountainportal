<?php
/**
 * @package		Joomla.Site
 * @subpackage	mod_stats
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;
unset($list[0]);
?>
<ul class="stats-module<?php echo $moduleclass_sfx ?>">
<?php foreach ($list as $item) : ?>
	<li><?php echo $item->title;?> 
	<strong><?php echo $item->data;?></strong></li>
<?php endforeach; ?>
</ul>
