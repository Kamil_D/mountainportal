<?php 
defined( '_JEXEC' ) or die( 'Restricted access' );
define( 'YOURBASEPATH', dirname(__FILE__) );
$cssVersion = '1.16';
$jsVersion = '1.15'; 
 
$app = JFactory::getApplication();

$document =&JFactory::getDocument();
$domain=$app->getCfg('live_site');
$mediaDomain='http://www.portalgorski.pl/';
$option = JRequest::getCmd('option');

$canonical = & JURI::current();
$canonical = ltrim( $canonical, 'http://www'); 
$canonical = preg_replace('~^/+|/+$|/(?=/)~', '-', $canonical); 
$canonical = 'http://www'.$canonical;
$canonical = rtrim($canonical,"/"); 

$canonicalTag = '<link rel="canonical" href="'.$canonical.'"/>';
  
//pkk Set a proper title:
$title = $this->getTitle();
$this->setTitle(' » '. $title);

    if($app->isSite())
    {
	//pkk zarzadzanie css 

	//$document->addStyleSheet($domain.'templates/'.$this->template.'/css/jq.css?v='.$cssVersion,'text/css');
	$document->addStyleSheet($domain.'templates/'.$this->template.'/css/style.css?v='.$cssVersion,'text/css','screen');
	//$document->addStyleSheet($domain.'templates/'.$this->template.'/css/style-mobile.css?v='.$cssVersion,'text/css','screen and (max-width: 640px)');
	if ($option=='com_climbinggym') {
		$document->addStyleSheet($mediaDomain.'components/com_climbinggym/assets/css/gymStyle.css?v='.$cssVersion);   
		$document->addStyleSheet($mediaDomain.'plugins/content/extravote/assets/extravote.css?v='.$cssVersion);

	}	 
	//pkk zarządzanie js
	if($option!='com_joomblog') {
	    unset($document->_scripts['/media/system/js/mootools-core-uncompressed.js']);
        unset($document->_scripts['/media/system/js/mootools-core.js']);
        unset($document->_scripts['/media/system/js/mootools-more-uncompressed.js']);
        unset($document->_scripts['/media/system/js/mootools-more.js']);
        unset($document->_scripts['/media/system/js/modal.js']);
        unset($document->_scripts['/media/system/js/caption.js']);
        unset($document->_scripts['/media/system/js/core.js']);
        unset($document->_scripts['/media/system/js/core-uncompressed.js']);
        unset($document->_scripts['/media/system/js/multiselect.js']);
        unset($document->_scripts['/media/system/js/caption-uncompressed.js']);
        unset($document->_scripts['/media/system/js/modal-uncompressed.js']);
        unset($document->_scripts['/media/system/js/modal.js']);
	}
        unset($document->_scripts[$domain. 'components/com_joomblog/libraries/jw_disqus/includes/js/mootools-core-1.4.5-full-compat.js']);
        unset($document->_scripts[$domain. 'components/com_joomblog/libraries/jw_disqus/includes/js/behaviour.js']);
        unset($document->_scripts[$domain. 'components/com_joomblog/libraries/jw_disqus/includes/js/behaviour.js']);
        unset($document->_scripts[$domain. 'components/com_joomblog/js/jquery-1.8.3.min.js']);
        unset($document->_styleSheets[$domain.'components/com_joomblog/templates/default/css/template_style.css']);
    // JS DEBUG:
    //echo '<!--<pre>';
	//var_dump($document->_styleSheets,$this->baseurl .'/components/com_joomblog/templates/default/css/template_style.css');
	//echo '</pre>-->'; 
	$tmp_script=array($domain.'/templates/'.$this->template.'/js.php?v=1'.$jsVersion => array());
	$document->_scripts=$tmp_script + $document->_scripts;
    }
  
?>
 <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
 <head>	
   		<?php if($option=='com_content') { echo $canonicalTag;} ?>
		<link rel="alternate" type="application/rss+xml" title="PortalGorski.pl" href="http://portalgorski.pl/nowosci?format=feed&type=rss" />
		<meta name="viewport" content="user-scalable=no,width=device-width" />
		<meta name="google-site-verification" content="IYE7sSRv83TQ8FYz0NzE4OuLm77SHm7p1PMvtq0NcBk" />
		<jdoc:include type="head" />
        <!--[if lt IE 9]> <link rel="stylesheet" href="<?php echo $domain.$this->baseurl; ?>/templates/jkw/css/ie.css" type="text/css" />
        <script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE8.js">var IE7_PNG_SUFFIX = ".png";</script><![endif]--> 
 </head>
 <body class="background">
	<div id="main" >
	    	<div id="header" >
			<div class="logo_container">
				<a href="<?php echo $this->baseurl ?>/" title="mountainportal.com - home">
					<!--
                                    <img src="<?php echo $mediaDomain; ?>/templates/<?php echo $this->template; ?>/images/logo_big.png" 
					     border="0" class="logo" alt="PortalGorski.pl - Portal Górski - Informacje z Gór" />
				-->
                                <img src="templates/jkw/images/logo_big_ENG2.png" 
					     border="0" class="logo" alt="mountainportal.com - Mountain Portal  " />
                                </a>
			</div>
		<div class="left">
			<jdoc:include type="modules" name="position-0" style="none" />
		</div>
		<div class="right" >
<!--			<div class="master-menu">
			<ul class="menu nav"><li>
				<a href="http://www.portalgorski.pl/" title="PortalGorski.pl"> Polish Version</a>
			</li></ul>
				
			</div>-->


<div class="flags">   
    <a href="http://www.portalgorski.pl/" title="PortalGorski.pl"></a>
</div>
                    
                    <jdoc:include type="modules" name="top-right" style="none" />
		</div>
		
        	<?php if ($this->countModules('top')) :  
                ?><div class="top">
                    <jdoc:include type="modules" name="top" style="none"/>
                </div>
			<?php endif;                         
		?> </div> 
		<div id="wrapper">
			<div id="navr">
				<div id="navl">
					<div id="nav">
						<div id="nav-left">
						    <jdoc:include type="modules" name="menuload" style="none" />
						   
						</div>
						<div class="right login-menu-container">
                            <?php
								$guest = 0;   
								$joomla_user= JFactory::getSession()->get('user');
								if($joomla_user) {
									$guest=$joomla_user->get('id',0);
								}
								if($guest==0) { ?>
									<jdoc:include type="modules" name="menu-not-logged" style="none" />
                            	<?php } else { ?>
                                  <img class="tt" style="float: left;margin-top: 2px;max-height: 30px;max-width: 30px;" title="Jesteś zalogowany jako <?php echo $joomla_user -> username;?>"
                                    src="http://id.portalgorski.pl/index.php?co=44&akcja=pokaz&id=<?php echo md5('_.34*m@Y3s' . $joomla_user -> username) ?>" 
                                    alt="avatar - <?php echo $joomla_user -> username;?>" />
                                  <jdoc:include type="modules" name="menu-logged" style="none" />  
                              <?php } ?>
                                
                        </div>
					</div>
				</div>
			</div>
			<div id="main-content">
				<?php if ($this->countModules('breadcrumbs')) : ?>
					<jdoc:include type="modules" name="breadcrumbs"  style="none"/>
				<?php endif; ?>
				<div class="clearpad">	</div>
				<div id="message">
				    <jdoc:include type="message" />
				</div>   
				<div id="centercontent_bg">
					<div class="clearpad"></div>
					<?php if (in_array($option,array('com_climbinggym','com_mmaps'))) { ?>
					    <jdoc:include type="component" />
						<br class="clr"/>
						<jdoc:include type="modules" name="gyms_footer"  style=""/>
					<?php }elseif($option=='com_users') { 
					    $app->enqueueMessage('Nazwa użytkownika/hasło nie zostały znalezione lub są nieaktywne.', 'warning'); 
						if(JRequest::getCmd('view')==login) {
							$app->redirect($app -> getCfg('live_site'),'');
						}
					} else { ?>
						<div  id="right" style="">
							<?php if(JRequest::getVar('view') != "featured") { ?>
								<div class="banner"></div>
								<jdoc:include type="modules" name="right"  style=""/>

							<?php }?>
							
							<!-- comp-mod -->
							<jdoc:include type="component" />
							<div class="banner"></div>
							<script type="text/javascript">
								
								jQuery('div.banner').each(function(){
									var el = jQuery(this);
								 	jQuery.ajax({
    									url: "http://www.portalgorski.pl/index.php?option=com_frontpage&task=baner.show&format=raw&position=topo_top&rand=",
    									cache: false,
    									dataType: "html",
    									success: function(data) {
        									el.html(data);
    									}
									});
									
								});
							</script>
						<?php
							if(JRequest::getVar('view') == "featured" ){
								?><jdoc:include type="modules" name="frontpage"  style=""/> <?php
							}
						
							if(JRequest::getVar('view') == "article" ){
								?><jdoc:include type="modules" name="after_article"  style="xhtml"/> <?php
							}
						?>
						</div>
						<div  id="left" >
						 	<div class="banner-horizontal-1"></div>
						    <jdoc:include type="modules" name="left"  style="xhtml"/>
						    <div class="banner-horizontal-2"></div>
							
						</div>
					<?php } ?>	
						<div class="clr"></div>
				</div>	
				<div class="clr"></div>
				<div id="after-content">
					
				</div>
			</div>
			<div id="footer" class="downshadow">
				<small class="left">Mountain Portal - mountainportal.com</small>
				<div class="right">
					<a href="https://plus.google.com/u/0/b/102730365855936239905/" target="_blank" title="Zobacz nasz profil na Google +">
    					<img height="25px;" src="<?php echo $mediaDomain.'/templates/'.$this->template;?>/images/g+_25.png" alt="portal gorski rss feed" /></a>
    				<a href="http://www.facebook.com/pages/JKWPL-Portal-Górski/115504368517205" target="_blank" title="Zobacz nasz profil na Facebook">
    					<img height="25px;" src="<?php echo $mediaDomain.'/templates/'.$this->template;?>/images/facebook-logo-25.png" alt="portal gorski rss feed" /></a>
    				<a href="/nowosci?format=feed&amp;type=rss" target="_blank" title="Subskrybuj nasze News'y o wspinaczce i Górach">
    					<img height="25px;" src="<?php echo $mediaDomain.'/templates/'.$this->template;?>/images/rssbutton.png" alt="portal gorski rss feed" /></a>
    				<a href="http://vimeo.com/user9039205/videos" target="_blank">
    					<img height="25px;" src="<?php echo $mediaDomain.'/templates/'.$this->template;?>/images/vimeo.png" alt="portal gorski vimeo account" /></a>
    				<a href="https://play.google.com/store/apps/details?id=pl.portalgorski.android" title="Wypróbuj naszą górską aplikację na androida" target="_blank">
    					<img height="25px;" src="<?php echo $mediaDomain.'/templates/'.$this->template;?>/images/android_icon.jpg" alt="portal gorski android app" /></a>
    				</a>
				</div>
				<div class="clr"></div>
				<!--<a href="http://stat.4u.pl/?nandadevi" target="_blank">			<img  class="footer-logo" src="http://adstat.4u.pl/s4u.gif" border="0" /></a>-->
					<div style="visibility:hidden">
						<script type="text/javascript">//pkk
							
						</script>
						<noscript><img alt="" src="http://stat.4u.pl/cgi-bin/s.cgi?i=nandadevi&r=ns" width="1" height="1"></noscript>
					</div>
					<div >
						<p>
						<small>Find Us: 
							<a href="http://www.jkw.pl" >www.jkw.pl</a>, <a href="http://www.portalgorski.pl" >www.portalgorski.pl</a>, <a href="http://www.himalaizm.pl" >www.himalaizm.pl</a>,
							<a href="http://www.serwiswspinaczkowy.pl" >www.serwiswspinaczkowy.pl</a>, <a href="http://www.jkw.com.pl" >www.jkw.com.pl</a>,
							<a href="http://www.serwiswspinaczkowy.pl" >www.serwiswspinaczkowy.pl</a>, <a href="http://www.jkw.com.pl" >www.jkw.com.pl</a>,
							<a href="http://www.jurajskiklubwysokogorski.pl" >www.jurajskiklubwysokogorski.pl</a>, <a href="http://www.jkw.org.pl " >www.jkw.org.pl </a>
						</small>
						<p> 
					</div> 		
					<br/>	
					<div style="display:none;">
					    <jdoc:include type="modules" name="footer"  style=""/>
					</div>
			</div>
		</div>   		
	</div>
	<jdoc:include type="modules" name="debug" />   
	<jdoc:include type="modules" name="facebook"  style=""/>
	<script type="text/javascript">
	   var _gaq = _gaq || [];  _gaq.push(['_setAccount', 'UA-28253216-1']);  _gaq.push(['_trackPageview']);  (function() {    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;   ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);  })();
	</script>
 </body>
</html>
