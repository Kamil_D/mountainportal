(function ($) {

  // Log all jQuery AJAX requests to Google Analytics
  jQuery(document).ajaxSend(function(event, xhr, settings){ 
    if (typeof _gaq !== "undefined" && _gaq !== null) {
      _gaq.push(['_trackPageview', settings.url]);
    }
  });

})(jQuery);

jQuery(document).ready(function() { 
    try {
        jQuery('.item-page a[target=_blank] img').parent("a").addClass('jcepopup autopopup-multiple');
        jQuery('.item-page a[target=_blank] img').parent("a").attr('rel','group[article]') ;
        jQuery('.item-page a[href*="displayimage.php"] img').parent("a").removeClass('jcepopup autopopup-multiple');
		jQuery('.item-page a.new-window').removeClass('jcepopup autopopup-multiple');
        jQuery('.extravote-count').append('<a class=\"Ttip tt\" href=\"#\" title=\"Kliknij w odpowiednią gwiazdkę by oddać swój głos.\">?</a>');
        jQuery('.tt').aToolTip({      });       
        jQuery('img.tt.type_icon').aToolTip({fixed: false});  
        jQuery('.icons img.tt').aToolTip({fixed: true,xOffset: -32,yOffset: 5});
        jQuery("#newsletter_form input[type=submit]").click(function() {   
        var url = "/newsletter/user/process.php"; // the script where you handle    the form input.
        jQuery.ajax({   type: "POST", url: url, data: jQuery("#newsletter_form").serialize(), // serializes the form's    elements.
            success: function(data) {
                    jQuery('#newsletter_form').css('display','none').parent().html('Na podany adres wysłaliśmy email potwierdzający.');
                }
            });
            return false; 
        });
        jQuery('#form-login').dialog({ autoOpen: false, modal:true}); 
        jQuery('a.login-dialog').on("click",function(event) {
         jQuery( "#form-login" ).dialog( "open" );
        });

        jQuery("#nav ul.menu:first-child").tinyNav({active: 'current',  label: 'Menu : '});
        jQuery("#nav ul.menu:last-child").tinyNav({active: 'current',  label: 'Użytkownik : '});
        jQuery('#cpg_photos').load('index.php?option=com_frontpage&view=cpg',function(){
        jQuery('#cpg_photos ul').simplyScroll({auto: false, manualMode: 'end',      frameRate: 20,      speed:5});
        });

    }
  catch(e) {console.log(e);}

});


jQuery(window).load(function() {
	try {  
    //Joomla keep alive behaviour
   /* (function worker() {
      jQuery.get('index.php?option=com_frontpage&limit=5&view=list&catid=104', function(data) {

        setTimeout(worker, 360000);
    });
  })();
*/
  displayCookieNotice();
  /*var baner_counter = 0;
  jQuery('.baner-horizontal').each(function(){
      baner_counter++;
      var url = 'http://www.portalgorski.pl/index.php?option=com_frontpage&task=baner.show&format=raw&position=baner-horizontal-'+baner_counter;
      var baner_container = jQuery(this);
      baner_container.load(url, function(responseText, textStatus, XMLHttpRequest) {

        if(responseText.length>0) {
          baner_container.addClass('loaded').prev().addClass('slim').prev('.shadowed_title').addClass('pushed');
        }
        
      });
      
  });
*/
  jQuery('#fb_frame').attr('src', jQuery('#likebox_1').attr('rel'));
} catch(e) { 
  console.log(e);
}

});

function displayCookieNotice() {
	if(jQuery.cookie('the_cookie')!=1) {
     jQuery('body').append('<a class="cookies" >xxx</a>');

     jQuery('a.cookies').css('position','absolute').css('left', jQuery(window).width()/2).css('top', '10px').css('visibility','hidden');
     jQuery('a.cookies').aToolTip({clickIt: true, fixed:true,xOffset: -255, yOffset: -60, 
       onHide: function(){ jQuery.cookie('the_cookie', '1',{ expires: 1365, path: '/' });} ,
       tipContent: 'This site uses cookies. By continuing to browse the site, you are agreeing to our use of cookies.'  
   }); 
     jQuery('a.cookies').trigger('click'); 
 } 
}


