<?php   
ob_start('ob_gzhandler');
	header("Content-type: application/x-javascript");
$page_data=0;	
	$last_mod=gmdate('D, d M Y H:i:s',$page_data). ' GMT';
	header("Pragma: "); // session_start daje "no-cache" więc trzeba wyczyścić.
	header("Cache-Control: max-age=604800, must-revalidate, post-check=11600, pre-check=811800");
	header('Last-Modified: ' . $last_mod);
	header('Expires: ' . gmdate('D, d M Y H:i:s', time() + 3600*8*24) . ' GMT');
	if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) 
		&& trim($_SERVER['HTTP_IF_MODIFIED_SINCE'])==$last_mod) {
			header('HTTP/1.0 304 Not Modified');
	//		header('Content-Length: 0');
	//		header('Status: 304 Not Modified');
	// na serwerach home.pl nie działa HTTP/1.1, trzeba robić przez Status.
			die();
	}
	$js_buff='';
	$js_buff.=file_get_contents('js/jq.min.js');
	$js_buff.=file_get_contents('js/jq.ui.min.js');
	$js_buff.=file_get_contents('js/jq.ui.sel.min.js');
	$js_buff.=file_get_contents('js/tt.min.js');
	$js_buff.=file_get_contents('js/pkk.js');
	echo $js_buff;
	
	
ob_end_flush();
exit; ?>
