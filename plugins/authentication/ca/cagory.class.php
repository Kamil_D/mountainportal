<?php
/** V.230
 * Przykład klasy obsługującej API Cagory - zdalne polaczenia
 * @author Krzysztof Trzcinka
 * @version 1.0.0 2012-10-29
 */
class Cagory 
    {
        protected $domenaCagory = NULL;
        protected $autoryzacjaLogin = NULL;
        protected $autoryzacjaHaslo = NULL;
        protected $adresPolaczenia = NULL;
    
        public function __construct($domenaCagory,$autoryzacjaLogin,$autoryzacjaHaslo)
            {
                $this->domenaCagory = $domenaCagory;
                $this->autoryzacjaLogin = $autoryzacjaLogin;
                $this->autoryzacjaHaslo = $autoryzacjaHaslo;
            }

       /*
        * Metoda realizująca połączenie przy pomocy curl
        * @param array $parametry - tablica parametrów do przesłania metodą POST
        * @return array - metoda zwraca tablice juz po deserializacji
        */        
        protected function polaczenie($parametry)
            {
                $login = $this->autoryzacjaLogin;
                $haslo = $this->autoryzacjaHaslo;
                
                $polaczenie = curl_init();
                curl_setopt($polaczenie, CURLOPT_USERPWD, "$login:$haslo");
                curl_setopt($polaczenie, CURLOPT_URL, $this->adresPolaczenia);
                curl_setopt($polaczenie, CURLOPT_POST, TRUE);
                curl_setopt($polaczenie, CURLOPT_POSTFIELDS, $parametry);
                curl_setopt($polaczenie, CURLOPT_RETURNTRANSFER, TRUE);
                $dane = curl_exec($polaczenie);
                curl_close($polaczenie);
                
                $rezultat = @unserialize($dane);
                if ($rezultat)
                    { return $rezultat; }
                else
                    { return $dane; }
            }

       /*
        * Metoda realizuje akcje "autoryzacja", ustawia parametry i wywołuje połączenie z odpowiednim adresem 
        * @param string $haslo - hasło użytkownika do autoryzacji
        * @param string $login - login użytkownika do autoryzacji
        * @return array - metoda zwraca tablice zwracana przez metodę $this->polaczenie($parametry)
        */ 
        public function autoryzacjaUzytkownika($login,$haslo)
            {
                $parametry['login'] = $login;
                $parametry['haslo'] = $haslo;
                
                $this->adresPolaczenia = $this->domenaCagory.'/index_curl.php?co=38&akcja=autoryzacja';
                
                return $this->polaczenie($parametry);
            }

       /*
        * Metoda realizuje akcje "pobieranie", ustawia parametry i wywołuje połączenie z odpowiednim adresem 
        * @param integer $id - id uztkownika, ktrego dane chcemy pobrać
        * @return array - metoda zwraca tablice zwracana przez metodę $this->polaczenie($parametry)
        */ 
        public function daneUzytkownika($id)
            {
                $parametry['id'] = $id;
                $this->adresPolaczenia = $this->domenaCagory.'/index_curl.php?co=38&akcja=pobieranie';
                
                return $this->polaczenie($parametry);
            }
            
       /*
        * Metoda realizuje akcje "rejestrowanie", ustawia parametry i wywołuje połączenie z odpowiednim adresem 
        * @param array $daneUzytkownika - tablica parametrów określających pola użytkownika (* pola wymagane to: mail, login, haslo)
        * @return array - metoda zwraca tablice zwracana przez metodę $this->polaczenie($parametry)
        */ 
        public function rejestracjaUzytkownika($daneUzytkownika)
            {
                $parametry = $daneUzytkownika;
                $this->adresPolaczenia = $this->domenaCagory.'/index_curl.php?co=38&akcja=rejestrowanie';
                
                return $this->polaczenie($parametry);
            }
            
       /*
        * Metoda realizuje akcje "modyfikowanie", ustawia parametry i wywołuje połączenie z odpowiednim adresem 
        * @param array $daneUzytkownika - tablica parametrów określających pola użytkownika (* pola wymagane to: id, mail)
        * @return array - metoda zwraca tablice zwracana przez metodę $this->polaczenie($parametry)
        */ 
        public function modyfikacjaUzytkownika($daneUzytkownika)
            {
                $parametry = $daneUzytkownika;
                $this->adresPolaczenia = $this->domenaCagory.'/index_curl.php?co=38&akcja=modyfikowanie';
                
                return $this->polaczenie($parametry);
            }
    }
?>