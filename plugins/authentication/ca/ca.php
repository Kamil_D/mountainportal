<?php

/**
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die ;

/**
 * CA Authentication Plugin
 *
 * @package		Joomla.Plugin
 * @subpackage	Authentication.ca
 * 
 */
class plgAuthenticationCa extends JPlugin {
	/**
	 * This method handles only authentication, creating user and user data synchronization is  handled by the system plugin
	 *
	 * @access	public
	 * @param   array	$credentials Array holding the user credentials
	 * @param	array   $options	Array of extra options
	 * @param	object	$response	Authentication response object
	 * @return	boolean
	 * @since 1.5
	 */
	function onUserAuthenticate($credentials, $options, &$response) {

		$app = JFactory::getApplication();
		if ($app -> isSite()) {
			require_once (JPATH_BASE . DS . 'plugins' . DS . 'authentication' . DS . 'ca' . DS . 'cagory.class.php');
		} else {
			//pkk dostep do administracji jak narazie wylaczamy poza CA
			return false;
		}

		$success = 0;
		$message = '';

		// check if we have curl or not
		if (function_exists('curl_init')) {

			$ca_domain = "http://id.portalgorski.pl/";
			$curl_login = 'Test';
			$curl_pass = 'nowehaslo';
			$cagory = New Cagory($ca_domain, $curl_login, $curl_pass);

			$curl_response = $cagory -> autoryzacjaUzytkownika($credentials['username'], $credentials['password']);

			if ($curl_response['status']) {

				$session = JFactory::getSession();

				$dane = $cagory -> daneUzytkownika($curl_response['dane']['id']);

				$session -> set('ca_id', $curl_response['dane']['id']);

				$user = JUser::getInstance($dane['dane']['login']);
				// Bring this in line with the rest of the system
				if (!$user) {

					$user = new JUser();
					$user -> set('id', 0);
					$user -> set('usertype', 'deprecated');

				}
				if (empty($dane['dane']['imie']) && (empty($dane['dane']['nazwisko']))) {
					$user -> set('name', $dane['dane']['login']);
				} else {
					$user -> set('name', $dane['dane']['imie'] . ' ' . $dane['dane']['nazwisko']);
				}
				$user -> set('ca_id', $curl_response['dane']['id']);
				$user -> set('lastvisitDate', $dane['dane']['ostatnie_logowanie']);
				$user -> save();

				$session -> set('user', $user);
				$response -> username = $dane['dane']['login'];
				$response -> email = $dane['dane']['mail'];
				$response -> name = $dane['dane']['imie'] . ' ' . $dane['dane']['nazwisko'];
				$response -> status = JAuthentication::STATUS_SUCCESS;
				$response -> error_message = '';

			} else {
				$response -> status = JAuthentication::STATUS_FAILURE;
				$response -> error_message = JText::_('JGLOBAL_AUTH_NO_USER');
			}
		} else {
			$app -> redirect($app -> getCfg('livesite'));
		}

	}

}
